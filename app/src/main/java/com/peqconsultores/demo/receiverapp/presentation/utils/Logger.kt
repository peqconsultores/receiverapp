package com.peqconsultores.demo.receiverapp.presentation.utils

import android.util.Log

/**
 * Created by jesusabv93 on 18/04/2019.
 */
interface Logger {
    val TAG: String
        get() = javaClass.simpleName

    fun d(message: String) = Log.e(TAG, message)
}