package com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit

import com.peqconsultores.demo.receiverapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import java.util.concurrent.TimeUnit.MILLISECONDS

/**
 * Created by jesusabv93 on 29/04/2019.
 */
object OkHttpClientProvider {

    fun getClient() = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) BODY else NONE
        })
        .readTimeout(30000, MILLISECONDS)
        .connectTimeout(30000, MILLISECONDS)
        .writeTimeout(30000, MILLISECONDS)
        .build()
}