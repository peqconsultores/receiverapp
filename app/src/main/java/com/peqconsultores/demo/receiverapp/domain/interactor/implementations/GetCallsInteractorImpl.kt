package com.peqconsultores.demo.receiverapp.domain.interactor.implementations

import com.peqconsultores.demo.receiverapp.data.repository.CallRepository
import com.peqconsultores.demo.receiverapp.domain.interactor.GetCallsInteractor

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class GetCallsInteractorImpl(private val callRepository: CallRepository) : GetCallsInteractor {
    override suspend fun invoke(origin: String) =
        callRepository.getCalls(origin)
}