package com.peqconsultores.demo.receiverapp.data.repository

import com.peqconsultores.demo.receiverapp.data.datastore.exception.AuthException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.domain.models.Auth

/**
 * Created by jesusabv93 on 18/04/2019.
 */
interface AuthRepository {
    @Throws(AuthException::class, NetworkException::class, ParseException::class)
    fun auth(identity: String, displayName: String): Auth
}