package com.peqconsultores.demo.receiverapp.data.datastore.rest

import android.content.Context
import com.google.gson.JsonSyntaxException
import com.peqconsultores.demo.receiverapp.data.datastore.CallDataStore
import com.peqconsultores.demo.receiverapp.data.datastore.exception.CallException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.CallResponse
import com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit.LipigasApiProvider
import java.net.ConnectException
import java.net.UnknownHostException

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class CallRestDataStore(val context: Context) : CallDataStore {

    override fun getCalls(origin: String): List<CallResponse> {
        try {
            val response = LipigasApiProvider.apiService.getCalls(origin).execute()
            when (response.code()) {
                200 -> {
                    try {
                        val rsp = response.body()
                        return if (rsp?.statusCode == 1) rsp.list
                        else throw CallException(rsp?.message ?: "No ha sido posible obtener las llamadas")
                    } catch (e: Exception) {
                        throw CallException("No ha sido posible obtener las llamadas")
                    }
                }
                422 -> throw CallException("No ha sido posible obtener las llamadas")
                500 -> throw CallException("No ha sido posible obtener las llamadas")
                401 -> throw CallException("No ha sido posible obtener las llamadas")
                403 -> throw CallException("No ha sido posible obtener las llamadas")
                else -> throw CallException("No ha sido posible obtener las llamadas")
            }
        } catch (ex: JsonSyntaxException) {
            throw ParseException("No ha sido posible obtener las llamadas", ex)
        } catch (ex: UnknownHostException) {
            throw NetworkException("Revise su conexión a internet", ex)
        } catch (ex: ConnectException) {
            throw NetworkException("Revise su conexión a internet", ex)
        } catch (ex: Exception) {
            when (ex) {
                is CallException, is NetworkException -> throw ex
                else -> throw CallException("No ha sido posible obtener las llamadas")
            }
        }
    }

    override fun postCall(
        appOrigin: String,
        answered: Int,
        duration: Long,
        origin: String,
        destinationPhone: String,
        originPhone: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String
    ): Boolean {
        try {
            val response = LipigasApiProvider.apiService.postCall(
                appOrigin,
                answered,
                duration,
                origin,
                destinationPhone,
                originPhone,
                versionAppOrigin,
                order,
                client,
                terminal
            ).execute()
            when (response.code()) {
                200 -> {
                    try {
                        val rsp = response.body()
                        return if (rsp?.error.isNullOrEmpty()) true
                        else throw CallException(rsp?.message ?: "No ha sido posible crear la llamada")
                    } catch (e: Exception) {
                        throw CallException("No ha sido posible crear la llamada")
                    }
                }
                422 -> throw CallException("No ha sido posible crear la llamada")
                500 -> throw CallException("No ha sido posible crear la llamada")
                401 -> throw CallException("No ha sido posible crear la llamada")
                403 -> throw CallException("No ha sido posible crear la llamada")
                else -> throw CallException("No ha sido posible crear la llamada")
            }
        } catch (ex: JsonSyntaxException) {
            throw ParseException("No ha sido posible crear la llamada", ex)
        } catch (ex: UnknownHostException) {
            throw NetworkException("Revise su conexión a internet", ex)
        } catch (ex: ConnectException) {
            throw NetworkException("Revise su conexión a internet", ex)
        } catch (ex: Exception) {
            when (ex) {
                is CallException, is NetworkException -> throw ex
                else -> throw CallException("No ha sido posible crear la llamada")
            }
        }
    }
}