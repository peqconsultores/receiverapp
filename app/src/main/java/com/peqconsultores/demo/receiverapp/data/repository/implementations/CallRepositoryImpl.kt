package com.peqconsultores.demo.receiverapp.data.repository.implementations

import com.peqconsultores.demo.receiverapp.data.datastore.factory.CallFactory
import com.peqconsultores.demo.receiverapp.data.datastore.factory.DataStoreEnum.REST
import com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers.CallMapper
import com.peqconsultores.demo.receiverapp.data.repository.CallRepository
import com.peqconsultores.demo.receiverapp.domain.models.Call

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class CallRepositoryImpl(val factory: CallFactory) : CallRepository {
    lateinit var callMapper: CallMapper

    override fun getCalls(origin: String): List<Call> {
        return callMapper.map(factory.create(REST).getCalls(origin))
    }

    override fun postCall(
        appOrigin: String,
        answered: Int,
        duration: Long,
        origin: String,
        destinationPhone: String,
        originPhone: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String
    ): Boolean {
        return factory.create(REST).postCall(
            appOrigin,
            answered,
            duration,
            origin,
            destinationPhone,
            originPhone,
            versionAppOrigin,
            order,
            client,
            terminal
        )
    }
}