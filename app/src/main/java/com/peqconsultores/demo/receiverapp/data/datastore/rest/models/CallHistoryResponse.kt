package com.peqconsultores.demo.receiverapp.data.datastore.rest.models
import com.google.gson.annotations.SerializedName

/**
 * Created by jesusabv93 on 29/04/2019.
 */
data class CallHistoryResponse(
    @SerializedName("cantidad") val quantity: Int,
    @SerializedName("data") val list: List<CallResponse>,
    @SerializedName("mensaje") val message: String,
    @SerializedName("status_code") val statusCode: Int
)