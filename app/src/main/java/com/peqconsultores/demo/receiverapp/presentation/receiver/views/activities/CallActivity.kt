package com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.os.SystemClock
import android.os.SystemClock.elapsedRealtime
import androidx.core.content.ContextCompat
import com.peqconsultores.demo.receiverapp.R
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import com.peqconsultores.demo.receiverapp.presentation.receiver.CallPresenter
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.CallView
import kotlinx.android.synthetic.main.activity_call.*

class CallActivity : AppCompatActivity(), CallView {

    companion object {
        val CALL_RECEIVER = "callReceiver"
        val DURATION = "duration"
    }

    private var callPresenter: CallPresenter? = null
    private var shouldMute = true
    private var enabled = true
    private lateinit var handler: Handler
    private var duration = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)
        handler = Handler()
        callPresenter = CallPresenter(this, this)
        callPresenter?.onCreate(intent?.extras?.getParcelable<Parcelable>(CALL_RECEIVER) as? CallReceiver)
    }

    override fun setCaller(callReceiver: CallReceiver) {
        tvIdentity.text = callReceiver.client
        tvDestination.text = getString(R.string.row_call_call_label_phone, callReceiver.destinationPhone.takeLast(4))
    }

    override fun setState(state: String) {
        handler.post {
            tvState.text = state
        }
    }

    override fun setListeners() {
        handler.post {
            btnVolume.setOnClickListener { callPresenter?.mute(shouldMute) }
            btnHangup.setOnClickListener { callPresenter?.hangup() }
            btnSpeakerPhone.setOnClickListener { callPresenter?.speakerPhone(enabled) }
        }
    }

    override fun setEnableViews(state: Boolean) {
        handler.post {
            btnVolume.isEnabled = state
            btnHangup.isEnabled = state
            btnSpeakerPhone.isEnabled = state
        }
    }

    override fun setDrawableMute(state: Boolean) {
        handler.post {
            shouldMute = state
            btnVolume.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    if (state) R.drawable.ic_mic_black_24dp else R.drawable.ic_mic_off_black_24dp
                )
            )
        }
    }

    override fun onHangup(callReceiver: CallReceiver) {
        handler.postDelayed({
            setResult(Activity.RESULT_OK, Intent().apply {
                putExtra(CALL_RECEIVER, callReceiver)
                putExtra(DURATION, duration)
            })
            finish()
        }, 1000)
    }

    override fun setDrawableSpekerPhone(state: Boolean) {
        handler.post {
            enabled = state
            btnSpeakerPhone.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    if (state) R.drawable.ic_volume_up_black_24dp else R.drawable.ic_volume_mute_black_24dp
                )
            )
        }
    }

    override fun startChronometer() {
        handler.post {
            chronometer.base = elapsedRealtime()
            chronometer.start()
        }
    }

    override fun stopChronometer() {
        handler.post {
            chronometer.stop()
            val elapsedMillis = SystemClock.elapsedRealtime() - chronometer.base
            duration = elapsedMillis / 1000
        }
    }

    override fun onDestroy() {
        callPresenter?.onDestroy()
        super.onDestroy()
    }

}