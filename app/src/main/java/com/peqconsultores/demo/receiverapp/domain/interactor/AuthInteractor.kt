package com.peqconsultores.demo.receiverapp.domain.interactor

import com.peqconsultores.demo.receiverapp.data.datastore.exception.AuthException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.domain.models.Auth

/**
 * Created by jesusabv93 on 18/04/2019.
 */
interface AuthInteractor {

    @Throws(AuthException::class, NetworkException::class, ParseException::class)
    suspend operator fun invoke(identity: String, displayName: String): Auth
}