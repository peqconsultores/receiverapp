package com.peqconsultores.demo.receiverapp.data.datastore.exception

import java.lang.Exception

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class CallException(message: String) : Exception(message)