package com.peqconsultores.demo.receiverapp.data.datastore.factory

import android.content.Context
import com.peqconsultores.demo.receiverapp.data.datastore.factory.DataStoreEnum.REST
import com.peqconsultores.demo.receiverapp.data.datastore.rest.AuthRestDataStore

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class AuthFactory(private val context: Context) {

    fun create(dataStoreEnum: DataStoreEnum) = when (dataStoreEnum) {
        REST -> AuthRestDataStore(context)
        else -> AuthRestDataStore(context)
    }
}