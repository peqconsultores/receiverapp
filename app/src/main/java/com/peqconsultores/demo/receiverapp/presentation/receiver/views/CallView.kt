package com.peqconsultores.demo.receiverapp.presentation.receiver.views

import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver

/**
 * Created by jesusabv93 on 28/04/2019.
 */
interface CallView {
    fun setCaller(callReceiver: CallReceiver)
    fun setState(state: String)
    fun setListeners()
    fun setEnableViews(state: Boolean)
    fun setDrawableMute(state: Boolean)
    fun onHangup(callReceiver: CallReceiver)
    fun setDrawableSpekerPhone(state: Boolean)
    fun startChronometer()
    fun stopChronometer()
}