package com.peqconsultores.demo.receiverapp.presentation.callhistory.views

import com.peqconsultores.demo.receiverapp.domain.models.Call

/**
 * Created by jesusabv93 on 29/04/2019.
 */
interface CallHistoryView {
    fun setupRecyclerView()
    fun updateCalls(calls: List<Call>)
    fun hideProgress()
}