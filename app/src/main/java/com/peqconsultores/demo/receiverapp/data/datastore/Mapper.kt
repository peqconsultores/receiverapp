package com.peqconsultores.demo.receiverapp.data.datastore

/**
 * Created by jesusabv93 on 18/04/2019.
 */
abstract class Mapper<T1, T2> {

    abstract fun map(value: T1): T2

    fun map(values: Collection<T1>): List<T2> = values.map { map(it) }

}