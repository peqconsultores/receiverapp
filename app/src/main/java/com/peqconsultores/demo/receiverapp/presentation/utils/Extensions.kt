package com.peqconsultores.demo.receiverapp.presentation.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.format.DateUtils.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.lang.System.*
import java.util.*

/**
 * Created by jesusabv93 on 18/04/2019.
 */
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)


inline fun <reified T : Activity> Activity.startActivity() {
    startActivity(Intent(this, T::class.java))
}


inline fun <reified T : Activity> Activity.startActivity(body: Intent.() -> Unit) {
    startActivity(intentFor<T>(body))
}


inline fun <reified T : Activity> Activity.intentFor(body: Intent.() -> Unit) =
    Intent(this, T::class.java).apply(body)


fun Date.humanizeDate(context: Context) =
    getRelativeDateTimeString(context, currentTimeMillis() - time, SECOND_IN_MILLIS, WEEK_IN_MILLIS, FORMAT_ABBREV_ALL)


fun Long.humanizeDuration(context: Context) = "$this"
