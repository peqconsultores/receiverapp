package com.peqconsultores.demo.receiverapp.data.datastore.factory

import android.content.Context
import com.peqconsultores.demo.receiverapp.data.datastore.factory.DataStoreEnum.REST
import com.peqconsultores.demo.receiverapp.data.datastore.rest.CallRestDataStore

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class CallFactory(private val context: Context) {
    fun create(dataStoreEnum: DataStoreEnum) = when (dataStoreEnum) {
        REST -> CallRestDataStore(context)
        else -> CallRestDataStore(context)
    }
}