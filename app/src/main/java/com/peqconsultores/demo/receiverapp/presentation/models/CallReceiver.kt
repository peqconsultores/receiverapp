package com.peqconsultores.demo.receiverapp.presentation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by jesusabv93 on 28/04/2019.
 */
@Parcelize
data class CallReceiver(
    val origin: String,
    val driverCode: String,
    val destinationPhone: String,
    val appOrigin: String,
    val versionAppOrigin: String,
    val order: String,
    val client: String,
    val from: String,
    val terminal: String
) : Parcelable