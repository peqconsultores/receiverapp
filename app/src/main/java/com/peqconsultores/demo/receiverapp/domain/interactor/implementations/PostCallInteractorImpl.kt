package com.peqconsultores.demo.receiverapp.domain.interactor.implementations

import com.peqconsultores.demo.receiverapp.data.repository.CallRepository
import com.peqconsultores.demo.receiverapp.domain.interactor.PostCallInteractor

/**
 * Created by jesusabv93 on 01/05/2019.
 */
class PostCallInteractorImpl(private val callRepository: CallRepository) : PostCallInteractor {
    override suspend fun invoke(
        appOrigin: String,
        answered: Int,
        duration: Long,
        origin: String,
        destinationPhone: String,
        originPhone: String,
        versionAppOrigin: String,
        order: String,
        client: String,
        terminal: String
    ) =
        callRepository.postCall(
            appOrigin,
            answered,
            duration,
            origin,
            destinationPhone,
            originPhone,
            versionAppOrigin,
            order,
            client,
            terminal
        )
}