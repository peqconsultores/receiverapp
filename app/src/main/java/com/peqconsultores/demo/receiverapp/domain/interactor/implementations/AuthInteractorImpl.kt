package com.peqconsultores.demo.receiverapp.domain.interactor.implementations

import com.peqconsultores.demo.receiverapp.data.repository.AuthRepository
import com.peqconsultores.demo.receiverapp.domain.interactor.AuthInteractor

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class AuthInteractorImpl(private val authRepository: AuthRepository) : AuthInteractor {
    override suspend fun invoke(identity: String, displayName: String) =
        authRepository.auth(identity, displayName)
}