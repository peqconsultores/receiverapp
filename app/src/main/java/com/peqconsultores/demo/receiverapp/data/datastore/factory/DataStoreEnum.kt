package com.peqconsultores.demo.receiverapp.data.datastore.factory

/**
 * Created by jesusabv93 on 18/04/2019.
 */
enum class DataStoreEnum {
    REST, PREFERENCES, INFOBIP
}