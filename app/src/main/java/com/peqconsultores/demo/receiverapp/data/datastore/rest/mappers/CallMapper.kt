package com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers

import com.peqconsultores.demo.receiverapp.data.datastore.Mapper
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.CallResponse
import com.peqconsultores.demo.receiverapp.domain.models.Call

/**
 * Created by jesusabv93 on 29/04/2019.
 */
class CallMapper : Mapper<CallResponse, Call>() {
    override fun map(value: CallResponse) = Call(
        appOrigin = value.appOrigin,
        answered = value.answered == 1,
        createAt = value.createAt,
        duration = value.duration,
        callId = value.callId,
        origin = value.origin,
        destinationPhone = value.destinationPhone,
        originPhone = value.originPhone,
        versionAppOrigin = value.versionAppOrigin
    )
}