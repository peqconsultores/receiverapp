package com.peqconsultores.demo.receiverapp.presentation.receiver

import android.content.Context
import android.util.Log.*
import com.crashlytics.android.Crashlytics.log
import com.infobip.webrtc.sdk.api.InfobipRTC
import com.peqconsultores.demo.receiverapp.data.datastore.factory.AuthFactory
import com.peqconsultores.demo.receiverapp.data.datastore.rest.mappers.AuthMapper
import com.peqconsultores.demo.receiverapp.data.repository.implementations.AuthRepositoryImpl
import com.peqconsultores.demo.receiverapp.domain.interactor.implementations.AuthInteractorImpl
import kotlinx.coroutines.launch
import java.lang.Exception
import com.infobip.webrtc.sdk.impl.DefaultInfobipRTC
//import com.infobip.webrtc.sdk.api.RTCOptions
import com.infobip.webrtc.sdk.api.call.CallRequest
import com.infobip.webrtc.sdk.api.call.OutgoingCall
import com.infobip.webrtc.sdk.api.call.options.CallPhoneNumberOptions
import com.infobip.webrtc.sdk.api.event.CallEventListener
//import com.infobip.webrtc.sdk.api.event.RTCEventListener
import com.infobip.webrtc.sdk.api.event.call.CallErrorEvent
import com.infobip.webrtc.sdk.api.event.call.CallEstablishedEvent
import com.infobip.webrtc.sdk.api.event.call.CallHangupEvent
import com.infobip.webrtc.sdk.api.event.call.CallRingingEvent
//import com.infobip.webrtc.sdk.api.event.rtc.ConnectedEvent
//import com.infobip.webrtc.sdk.api.event.rtc.DisconnectedEvent
//import com.infobip.webrtc.sdk.api.event.rtc.ReconnectedEvent
//import com.infobip.webrtc.sdk.api.event.rtc.ReconnectingEvent
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.CallView
import com.peqconsultores.demo.receiverapp.presentation.utils.Scope
import kotlinx.coroutines.Dispatchers

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class CallPresenter(val context: Context, callView: CallView) : Scope by Scope.Impl() {

    companion object {
        val TAG: String = CallPresenter::class.java.simpleName
    }

    private var view: CallView? = null
    private var infobipRTC: InfobipRTC? = null
    private var outgoingCall: OutgoingCall? = null

    init {
        view = callView
    }

    private val authInteractor = AuthInteractorImpl(AuthRepositoryImpl(AuthFactory(context)).apply {
        authMapper = AuthMapper()
    })

    fun onCreate(callReceiver: CallReceiver?) {
        view?.setEnableViews(false)
        initScope()
        if (callReceiver == null) return
        view?.setCaller(callReceiver)
        view?.setListeners()
        call(callReceiver)
    }

    private fun call(callReceiver: CallReceiver) = launch(Dispatchers.IO)  {

        val token = try {
            authInteractor.invoke(callReceiver.client, callReceiver.client).token
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        } ?: return@launch

        val callRequest = CallRequest(token, context, callReceiver.destinationPhone, callEventListener(callReceiver))
        val callPhoneNumberOutgoingCall = CallPhoneNumberOptions.builder().from(callReceiver.from).build()
        outgoingCall = InfobipRTC.callPhoneNumber(callRequest, callPhoneNumberOutgoingCall)
    }

    private fun callEventListener(callReceiver: CallReceiver): CallEventListener {
        return object : CallEventListener {
            override fun onHangup(callHangupEvent: CallHangupEvent?) {
                view?.setState("Llamada finalizada")
                view?.setEnableViews(false)
                view?.stopChronometer()
                view?.onHangup(callReceiver)
            }
            override fun onEstablished(callEstablishedEvent: CallEstablishedEvent?) {
                view?.setState("Conectando llamada")
                view?.setEnableViews(false)
            }
            override fun onError(callErrorEvent: CallErrorEvent?) {
                log(DEBUG, TAG, "callErrorEvent: ${callErrorEvent?.reason}")
                view?.setState("Error conectando llamada")
                view?.setEnableViews(false)
            }
            override fun onRinging(callRingingEvent: CallRingingEvent?) {
                view?.setState("Llamada conectada")
                view?.setEnableViews(true)
                view?.startChronometer()
            }
        }
    }


    /*private fun call(callReceiver: CallReceiver) = launch(Dispatchers.IO) {
        val token = try {
            authInteractor.invoke(callReceiver.client, callReceiver.client)
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        } ?: return@launch

        infobipRTC =  DefaultInfobipRTC.getInstance(token.token, RTCOptions.builder().debug(true).build(), context)
       infobipRTC?.addEventListener(object : RTCEventListener {
            override fun onReconnected(reconnectedEvent: ReconnectedEvent?) {
                log(DEBUG, TAG, "onReconnected: ${reconnectedEvent?.toString()}")
            }

            override fun onConnected(connectedEvent: ConnectedEvent?) {
                view?.setState("Listo para conectar")
                outgoingCall = infobipRTC?.callPhoneNumber(
                    callReceiver.destinationPhone,
                    CallPhoneNumberOptions.builder().from(callReceiver.from).build()
                )
                outgoingCall?.addEventListener(object : CallEventListener {
                    override fun onHangup(callHangupEvent: CallHangupEvent?) {
                        view?.setState("Llama finalizada")
                        view?.setEnableViews(false)
                        view?.stopChronometer()
                        view?.onHangup(callReceiver)
                    }

                    override fun onEstablished(callEstablishedEvent: CallEstablishedEvent?) {
                        view?.setState("Llamada conectada")
                        view?.setEnableViews(true)
                        view?.startChronometer()
                    }

                    override fun onError(callErrorEvent: CallErrorEvent?) {
                        log(DEBUG, TAG, "callErrorEvent: ${callErrorEvent?.reason}")
                        view?.setState("Error conectando llamada")
                        view?.setEnableViews(false)
                    }

                    override fun onRinging(callRingingEvent: CallRingingEvent?) {
                        view?.setState("Conectando llamada")
                        view?.setEnableViews(false)
                    }
                })
            }

            override fun onDisconnected(disconnectedEvent: DisconnectedEvent?) {
                log(DEBUG, TAG, "disconnectedEvent: ${disconnectedEvent?.reason}")
            }

            override fun onReconnecting(reconnectingEvent: ReconnectingEvent?) {
                log(DEBUG, TAG, "onReconnecting: ${reconnectingEvent?.toString()}")
            }
        })
        infobipRTC?.connect()
    }*/

    fun mute(shouldMute: Boolean) {
        outgoingCall?.mute(shouldMute)
        view?.setDrawableMute(!shouldMute)
    }

    fun hangup() {
        outgoingCall?.hangup()
    }

    fun speakerPhone(enabled: Boolean){
        outgoingCall?.speakerphone(enabled)
        view?.setDrawableSpekerPhone(!enabled)
    }

    fun onDestroy() {
        //infobipRTC?.disconnect()
        view = null
        cancelScope()
    }

}