package com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities

import android.Manifest.permission.*
import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.OnClickListener
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.peqconsultores.demo.receiverapp.R
import com.peqconsultores.demo.receiverapp.presentation.callhistory.views.activities.CallsHistoryActivity
import com.peqconsultores.demo.receiverapp.presentation.models.CallReceiver
import com.peqconsultores.demo.receiverapp.presentation.receiver.ReceiverPresenter
import com.peqconsultores.demo.receiverapp.presentation.receiver.ReceiverView
import com.peqconsultores.demo.receiverapp.presentation.utils.startActivity
import kotlinx.android.synthetic.main.activity_receiver.*
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import android.content.Intent
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.net.Uri
import com.google.android.material.snackbar.Snackbar.*
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities.CallActivity.Companion.CALL_RECEIVER
import com.peqconsultores.demo.receiverapp.presentation.receiver.views.activities.CallActivity.Companion.DURATION

class ReceiverActivity : AppCompatActivity(), ReceiverView {

    companion object {
        val ORIGIN = "origin"
        val DRIVER_CODE = "driver_code"
        val DESTINATION_PHONE = "destination_phone"
        val APP_ORIGIN = "app_origin"
        val VERSION_APP_ORIGIN = "version_app_origin"
        val ORDER = "order"
        val CLIENT = "client"
        val TERMINAL = "terminal"
        val CALL_REQUEST_CODE = 100
    }

    private var snackBar: Snackbar? = null
    private var receiverPresenter: ReceiverPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receiver)
        receiverPresenter = ReceiverPresenter(this as Context, this)
        receiverPresenter?.onCreate(
            intent.data?.getQueryParameter(ORIGIN),
            intent.data?.getQueryParameter(DRIVER_CODE),
            intent.data?.getQueryParameter(DESTINATION_PHONE),
            intent.data?.getQueryParameter(APP_ORIGIN),
            intent.data?.getQueryParameter(VERSION_APP_ORIGIN),
            intent.data?.getQueryParameter(ORDER),
            intent.data?.getQueryParameter(CLIENT),
            intent.data?.getQueryParameter(TERMINAL)
        )
    }

    override fun setCallHistoryListener(callReceiver: CallReceiver) {
        btnCallsHistory.setOnClickListener {
            startActivity<CallsHistoryActivity> {
                putExtra(
                    CallsHistoryActivity.CALL_RECEIVER,
                    callReceiver
                )
            }
        }
    }

    override fun setHideCaller() {
        tvData.text = ""
        tvData.visibility = VISIBLE
        btnCall.visibility = GONE
    }

    override fun setCall(callReceiver: CallReceiver) {
        tvData.text = buildSpannedString {
            bold { append("Origen: ") }
            appendln(callReceiver.origin)
            appendln("")

            bold { append("Código de Chofer: ") }
            appendln(callReceiver.driverCode)
            appendln("")

            bold { append("Teléfono de Cliente: ") }
            appendln(getString(R.string.row_call_call_label_phone, callReceiver.destinationPhone.takeLast(4)))
            appendln("")

            bold { append("Aplicación origen: ") }
            appendln(callReceiver.appOrigin)
            appendln("")

            bold { append("Versión aplicación origen: ") }
            appendln(callReceiver.versionAppOrigin)
            appendln("")

            bold { append("Pedido: ") }
            appendln(callReceiver.order)
            appendln("")

            bold { append("Cliente: ") }
            appendln(callReceiver.client)
            appendln("")

            bold { append("Terminal: ") }
            appendln(callReceiver.terminal)
        }
        btnCall.tag = callReceiver
        btnCall.setOnClickListener(callOnClickListener)
        tvData.visibility = VISIBLE
        btnCall.visibility = VISIBLE
    }

    private val callOnClickListener = OnClickListener {
        Dexter.withActivity(this)
            .withPermission(RECORD_AUDIO)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    receiverPresenter?.onPermissionGranted(it.tag as CallReceiver)
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    showSnackBar()
                }
            })
            .check()
    }

    fun showSnackBar() {
        snackBar =
            make(rootView, R.string.permission_snackbar_message, LENGTH_LONG)
                .setAction(R.string.permission_snackbar_action) {
                    startActivity(
                        Intent(
                            ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.fromParts("package", packageName, null)
                        ).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        })
                }
        snackBar?.show()
    }

    override fun goToCallActivity(callReceiver: CallReceiver) {
        startActivityForResult(Intent(this, CallActivity::class.java).apply {
            putExtra(CALL_RECEIVER, callReceiver)
        }, CALL_REQUEST_CODE)
    }

    override fun onDestroy() {
        receiverPresenter?.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CALL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                receiverPresenter?.createCall(
                    data?.getParcelableExtra(CALL_RECEIVER)!!,
                    data?.getLongExtra(DURATION, 0L)!!
                )
            }
        }

    }

}