package com.peqconsultores.demo.receiverapp.presentation.callhistory.views.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.peqconsultores.demo.receiverapp.R
import com.peqconsultores.demo.receiverapp.domain.models.Call
import com.peqconsultores.demo.receiverapp.presentation.utils.humanizeDate
import com.peqconsultores.demo.receiverapp.presentation.utils.humanizeDuration
import com.peqconsultores.demo.receiverapp.presentation.utils.inflate
import kotlinx.android.synthetic.main.row_call.view.*

/**
 * Created by jesusabv93 on 11/04/2019.
 */
class CallAdapter(private val list: List<Call>) : RecyclerView.Adapter<CallAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int) = CustomViewHolder(viewGroup)

    override fun onBindViewHolder(customViewHolder: CustomViewHolder, i: Int) {
        customViewHolder.bind(list[i])
    }

    override fun getItemCount() = list.size

    inner class CustomViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        parent.inflate(R.layout.row_call)
    ) {
        private val tvPhone = itemView.tvPhone
        private val tvDate = itemView.tvDate
        private val tvDuration = itemView.tvDuration

        fun bind(call: Call) {
            with(call) {
                tvPhone.text = itemView.context.getString(R.string.row_call_call_label_phone, destinationPhone.takeLast(4))
                tvDate.text = createAt.humanizeDate(itemView.context)
                tvDuration.text = duration.humanizeDuration(itemView.context)
            }
        }
    }

}