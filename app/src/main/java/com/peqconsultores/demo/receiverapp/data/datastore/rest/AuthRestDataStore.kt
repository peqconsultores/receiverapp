package com.peqconsultores.demo.receiverapp.data.datastore.rest

import android.content.Context
import com.google.gson.JsonSyntaxException
import com.peqconsultores.demo.receiverapp.BuildConfig.Authorization
import com.peqconsultores.demo.receiverapp.data.datastore.AuthDataStore
import com.peqconsultores.demo.receiverapp.data.datastore.exception.AuthException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.AuthRequest
import com.peqconsultores.demo.receiverapp.data.datastore.rest.models.AuthResponse
import com.peqconsultores.demo.receiverapp.data.datastore.rest.retrofit.ApiProvider
import java.net.ConnectException
import java.net.UnknownHostException

/**
 * Created by jesusabv93 on 18/04/2019.
 */
class AuthRestDataStore(val context: Context) : AuthDataStore {

    override fun auth(identity: String, displayName: String): AuthResponse {
        try {
            val response = ApiProvider.apiService.auth(Authorization, AuthRequest(identity, displayName)).execute()
            when (response.code()) {
                200 -> {
                    try {
                        return response.body()!!
                    } catch (e: Exception) {
                        throw AuthException("No ha sido posible autenticarse")
                    }
                }
                422 -> throw AuthException("No ha sido posible autenticarse")
                500 -> throw AuthException("No ha sido posible autenticarse")
                401 -> throw AuthException("No ha sido posible autenticarse")
                403 -> throw AuthException("No ha sido posible autenticarse")
                else -> throw AuthException("No ha sido posible autenticarse")
            }
        } catch (ex: JsonSyntaxException) {
            throw ParseException("No ha sido posible autenticarse", ex)
        } catch (ex: UnknownHostException) {
            throw NetworkException("Revise su conexión a internet", ex)
        } catch (ex: ConnectException) {
            throw NetworkException("Revise su conexión a internet", ex)
        } catch (ex: Exception) {
            when (ex) {
                is AuthException, is NetworkException -> throw ex
                else -> throw AuthException("No ha sido posible autenticarse")
            }
        }
    }
}