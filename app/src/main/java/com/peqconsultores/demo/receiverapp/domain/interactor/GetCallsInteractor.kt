package com.peqconsultores.demo.receiverapp.domain.interactor

import com.peqconsultores.demo.receiverapp.data.datastore.exception.CallException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.NetworkException
import com.peqconsultores.demo.receiverapp.data.datastore.exception.ParseException
import com.peqconsultores.demo.receiverapp.domain.models.Call

/**
 * Created by jesusabv93 on 29/04/2019.
 */
interface GetCallsInteractor {

    @Throws(CallException::class, NetworkException::class, ParseException::class)
    suspend operator fun invoke(origin: String): List<Call>
}